<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace robote13\pages;

use Yii;

/**
 * Description of Bootstrap
 *
 * @author Tartharia
 */
class Bootstrap implements \yii\base\BootstrapInterface{
    public function bootstrap($app)
    {
        /** @var Module $module */
        if($app instanceof \yii\web\Application)
        {
            $module = $app->getModule('pages');
            
            if($module !== null && $module instanceof Module)
            {
                $rule = Yii::createObject([
                    'class' => \yii\web\GroupUrlRule::className(),
                    'prefix' => $module->urlPrefix,
                    'routePrefix' => 'pages',
                    'rules'  => $module->urlRules                    
                ]);
                
                Yii::$app->urlManager->addRules([$rule],false);
            }
        }
        
    }
}
