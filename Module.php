<?php

namespace robote13\pages;

/**
 * page module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'robote13\pages\controllers';
    
    /**
     * @inheritdoc
     */
    public $defaultRoute = 'frontend';
    
    /**
     * @var string The prefix for pages module URL.
     * @see \yii\web\GroupUrlRule::prefix
     */
    public $urlPrefix = 'page';
    
    /**
     * @var array The rules to be used in URL management.
     */
    public $urlRules = [
        "<page:[\w\d]+>" => "frontend/index"
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
