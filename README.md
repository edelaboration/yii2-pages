Yii2 static pages
=================
Module displays a static page from view-files

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist robote13/yii2-pages "*"
```

or add

```
"robote13/yii2-pages": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \robote13\pages\AutoloadExample::widget(); ?>```