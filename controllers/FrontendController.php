<?php

namespace robote13\pages\controllers;

use yii\web\Controller;
use yii\base\InvalidParamException;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `pages` module
 */
class FrontendController extends Controller
{
    public function actions() {
        return[
            'error'=> '\yii\web\ErrorAction'
        ];
    }

    /**
     * Renders the static pages
     * @return string
     */
    public function actionIndex($page = null)
    {
        if($page === null)
        {
            $page = 'index';
        }elseif($page == 'index'){
            $page = null;
        }
        
        try {
            $content = $this->render($page);
        } catch (InvalidParamException $exc) {
            throw new NotFoundHttpException();
        }
        
        return $content;
    }
}
